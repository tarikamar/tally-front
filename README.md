[![pipeline status](https://gitlab.com/tarikamar/tally-front/badges/master/pipeline.svg)](https://gitlab.com/tarikamar/tally-front/-/commits/master)
[![coverage report](https://gitlab.com/tarikamar/tally-front/badges/master/coverage.svg)](https://gitlab.com/tarikamar/tally-front/-/commits/master)

# Tally front

Based on nuxt so some documentation are the same

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```sh
npm install
```

Define environment:


```sh
#.env
NUXT_PUBLIC_API_BASE=http…
NUXT_PUBLIC_TRANSACTIONS_TABLE_ITEMS_PER_PAGE=20
NODE_TLS_REJECT_UNAUTHORIZED=0 # On dev only
```

## Development Server

Start the development server on `http://localhost:3000`:

```sh
npm run dev
```

## Test

```sh
npm run test
```

## Production

Build the application for production:

```sh
npm run build
```

Locally preview production build:

```sh
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
