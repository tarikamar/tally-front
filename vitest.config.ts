import {fileURLToPath} from 'node:url'
import {defineVitestConfig} from '@nuxt/test-utils/config'

export default defineVitestConfig({
  // any custom Vitest config i'm require
  test: {
    environment: 'nuxt'
  }
})
