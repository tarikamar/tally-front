// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  modules: [
    '@unocss/nuxt',
    '@nuxt/ui',
    '@nuxt/test-utils/module',
  ],

  runtimeConfig: {
    public: {
      apiBase: process.env.NUXT_PUBLIC_API_BASE,
      transactions: {
        table: {
          itemsPerPage: process.env.NUXT_PUBLIC_TRANSACTIONS_TABLE_ITEMS_PER_PAGE
        }
      }
    }
  },

  compatibilityDate: '2024-07-03'
})