import {beforeEach, describe, expect, test, vi} from 'vitest'

import { transactionsAPI } from "~/composables/API/apiTransactions";

global.$fetch = vi.fn()

const createFetchResponse = data => data

describe('transactionAPI', () => {
  beforeEach(() => {
    global.$fetch.mockReset()
  })

  describe('getAll', () => {
    test('getAll fetch with specific arguments', async () => {
      const transactions = [
        {
          id: 1,
          date: '2023-01-24',
          amount: -1
        },
        {
          id: 2,
          date: '2023-01-24',
          amount: -2
        }
      ]

      $fetch.mockResolvedValue(createFetchResponse(transactions))

      expect(await transactionsAPI.getAll()).toBe(transactions)
      expect($fetch).toHaveBeenCalledWith('/transactions')
    })
  })

  describe('addOne', () => {
    test('addOne without date must throw an error', async () => {
      await expect(() => transactionsAPI.addOne()).rejects.toThrowError('Missing date')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('addOne without amount must throw an error', async () => {
      await expect(() => transactionsAPI.addOne('2024-01-24'))
      .rejects.toThrowError('Missing amount')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('addOne without third party must throw an error', async () => {
      await expect(() => transactionsAPI.addOne('2024-01-24', 20))
      .rejects.toThrowError('Missing third party id')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('addOne fetch with specific arguments', async () => {
      const body = {
        date: '2023-01-24',
        amount: -2,
        third_party_id: 1
      }

      $fetch.mockResolvedValue(createFetchResponse(body))

      expect(
        await transactionsAPI.addOne(...Object.values(body))
      ).toBe(body)

      expect($fetch).toHaveBeenCalledWith(
        '/transactions',
        {method: 'POST', body}
      )
    })
  })

  describe('deleteOne', () => {
    test('deleteOne without id must throw an error', async () => {
      await expect(() => transactionsAPI.deleteOne())
        .rejects.toThrowError('Missing id')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('deleteOne fetch with specific arguments', async () => {
      const id = 3
      await transactionsAPI.deleteOne(id)

      expect($fetch).toHaveBeenCalledWith(
        `/transactions/${id}`,
        {
          method: 'DELETE'
        }
      )
    })
  })

  describe('editOne', () => {
    test('editOne without id must throw an error', async () => {
      await expect(() => transactionsAPI.editOne())
        .rejects.toThrowError('Missing id')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('editOne without body must throw an error', async () => {
      await expect(() => transactionsAPI.editOne(4))
        .rejects.toThrowError('Missing body')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('editOne without date must throw an error', async () => {
      await expect(() => transactionsAPI.editOne(4, {}))
        .rejects.toThrowError('Missing date')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('editOne without amount must throw an error', async () => {
      await expect(() => transactionsAPI.editOne(4, {date: '2024'}))
        .rejects.toThrowError('Missing amount')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('editOne without third party must throw an error', async () => {
      await expect(() => transactionsAPI.editOne(4, {date: '2024', amount: 30}))
        .rejects.toThrowError('Missing third party id')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('editOne fetch with specific arguments', async () => {
      const id = 3
      const body = {date: '2024', amount: -30, third_party_id: 1}
      await transactionsAPI.editOne(id, body)

      expect($fetch).toHaveBeenCalledWith(
        `/transactions/${id}`,
        {method: 'PUT', body}
      )
    })
  })
})
