import {beforeEach, describe, expect, test, vi} from 'vitest'

import { thirdPartiesAPI } from "~/composables/API/apiThirdParties"

global.$fetch = vi.fn()

const createFetchResponse = data => data

describe('thirdPartyAPI', () => {
  beforeEach(() => {
    global.$fetch.mockReset()
  })

  describe('getAll', () => {
    test('getAll fetch with specific arguments', async () => {
      const thirdParties = [
        {
          id: 1,
          name: 'One',
        },
        {
          id: 2,
          name: 'Two',
        }
      ]

      $fetch.mockResolvedValue(createFetchResponse(thirdParties))

      expect(await thirdPartiesAPI.getAll()).toBe(thirdParties)
      expect($fetch).toHaveBeenCalledWith('/third-parties')
    })
  })

  describe('addOne', () => {
    test('addOne without name must throw an error', async () => {
      await expect(() => thirdPartiesAPI.addOne()).rejects.toThrowError('Missing name')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('addOne fetch with specific arguments', async () => {
      const thirdParty = {
        id: 2,
        name: 'Three',
      }

      $fetch.mockResolvedValue(createFetchResponse(thirdParty))
      const body = {name: 'Three again'}

      expect(await thirdPartiesAPI.addOne(...Object.values(body))).toBe(thirdParty)
      expect($fetch).toHaveBeenCalledWith(
        '/third-parties',
        {method: 'POST', body}
      )
    })
  })

  describe('deleteOne', () => {
    test('deleteOne without id must throw an error', async () => {
      await expect(() => thirdPartiesAPI.deleteOne())
        .rejects.toThrowError('Missing id')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('deleteOne fetch with specific arguments', async () => {
      const id = 3
      await thirdPartiesAPI.deleteOne(id)

      expect($fetch).toHaveBeenCalledWith(
        `/third-parties/${id}`,
        {
          method: 'DELETE'
        }
      )
    })
  })

  describe('editOne', () => {
    test('editOne without id must throw an error', async () => {
      await expect(() => thirdPartiesAPI.editOne())
        .rejects.toThrowError('Missing id')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('editOne without body must throw an error', async () => {
      await expect(() => thirdPartiesAPI.editOne(4))
        .rejects.toThrowError('Missing body')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('editOne without name must throw an error', async () => {
      await expect(() => thirdPartiesAPI.editOne(4, {}))
        .rejects.toThrowError('Missing name')
      expect($fetch).not.toHaveBeenCalled()
    })

    test('editOne fetch with specific arguments', async () => {
      const id = 3
      const body = {name: 'Four'}
      await thirdPartiesAPI.editOne(id, body)

      expect($fetch).toHaveBeenCalledWith(
        `/third-parties/${id}`,
        {
          method: 'PUT',
          body
        }
      )
    })
  })
})
