import {test, expect} from 'vitest'
import { mountSuspended } from 'vitest-environment-nuxt/utils'

import TransactionFormSubmit from "~/components/Transaction/FormSubmit.vue"

test('FormSubmit create', async () => {
  const component = await mountSuspended(
    TransactionFormSubmit,
    {props: {edit: false}}
  )

  expect(component.findAll('.i-heroicons\\:paper-airplane-solid').length).toBe(1)
  expect(component.findAll('.i-heroicons\\:pencil-square-20-solid').length).toBe(0)
})

test('FormSubmit edit', async () => {
  const component = await mountSuspended(
    TransactionFormSubmit,
    {props: {edit: true}}
  )

  expect(component.findAll('.i-heroicons\\:paper-airplane-solid').length).toBe(0)
  expect(component.findAll('.i-heroicons\\:pencil-square-20-solid').length).toBe(1)
})
