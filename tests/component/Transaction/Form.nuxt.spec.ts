import {test, expect} from 'vitest'
import {mountSuspended} from 'vitest-environment-nuxt/utils'
import {beforeEach, describe, vi} from 'vitest'

import TransactionForm from "~/components/Transaction/Form.vue"

global.$fetch = vi.fn()

beforeEach(() => {
  global.$fetch.mockReset()
})

describe('Create', () => {
  test('Initial state', async () => {
    // Specify date
    const mockDate = new Date(2018, 11, 16)
    vi.setSystemTime(mockDate)

    const component = await mountSuspended(
      TransactionForm,
      {
        props: {
          transaction: undefined,
          thirdParties: undefined
        },
        provide: () => ({warnError: () => {}})
      }
    )

    const date = await component.find('[name="date"]').attributes('value')
    const today = (new Date()).toJSON().slice(0, 10)

    expect(date).toBe(today)
  })

  test('Submit empty form', async () => {
    const mockWarnError = vi.fn()
    const component = await mountSuspended(
      TransactionForm,
      {
        props: {
          transaction: undefined,
          thirdParties: undefined
        },
        provide: () => ({warnError: mockWarnError})
      }
    )

    await component.find('form').trigger('submit')

    expect(mockWarnError).toHaveBeenCalledWith({
      message: 'missing amount',
      title: 'Somme manquante'
    })
    expect($fetch).not.toHaveBeenCalled()
  })

  test('Submit missing third', async () => {
    const mockWarnError = vi.fn()
    const component = await mountSuspended(
      TransactionForm,
      {
        props: {
          transaction: undefined,
          thirdParties: undefined
        },
        provide: () => ({warnError: mockWarnError})
      }
    )

    component.find('[name="amount"]').setValue('-2')
    await component.find('form').trigger('submit')

    expect(mockWarnError).toHaveBeenCalledWith({
      message: 'missing third',
      title: 'Tier manquant'
    })
    expect($fetch).not.toHaveBeenCalled()
  })

  test.todo('Nominal', async () => {
    const mockWarnError = vi.fn()
    // renderSuspend dont return a dom utility (find not a function)
    const component = await mountSuspended(
      TransactionForm,
      {
        props: {
          transaction: undefined,
          thirdParties: [{id: 1, name: 'Un'}]
        },
        provide: () => ({warnError: mockWarnError})
      }
    )

    component.find('[name="amount"]').setValue('-4')

    // input name third-party is read-only
    // nuxt ui, create 2 other input names: third-party[id] & third-party[name]
    // those inputs are not ready with mountSuspended
    const inputThirdId = await component.find('[name="third-party[id]"]')
    await inputThirdId.setValue('1')

    const inputThirdName = await component.find('[name="third-party[name]"]')
    await inputThirdName.setValue('Un')

    await component.find('form').trigger('submit')

    expect(mockWarnError).not.toHaveBeenCalled()
    expect($fetch).not.toHaveBeenCalled()
  })
})
