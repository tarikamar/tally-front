import { mountSuspended } from 'vitest-environment-nuxt/utils'
import {test, expect} from 'vitest'

import TransactionPaginate  from '~/components/Transaction/Paginate.vue'

test('TransactionPaginate check default look', async () => {
  const component = await mountSuspended(TransactionPaginate, {
    props: {
      rowsPerPage: 2,
      totalRows: 6
    }
  })

  expect(component.text()).toBe('123')

  // Active page
  expect(component.findAll('.focus-visible\\:outline-primary-500').length).toBe(1)
  expect(component.find('.focus-visible\\:outline-primary-500').text()).toContain('1')

  //Arrows
  expect(component.find('[aria-label="Prev"]').attributes('disabled')).toBeDefined()
  expect(component.find('[aria-label="Next"]').attributes('disabled')).toBeUndefined()
})

test('TransactionPaginate click next page', async () => {
  const component = await mountSuspended(TransactionPaginate, {
    props: {
      rowsPerPage: 2,
      totalRows: 6
    }
  })

  await component.find('[aria-label="Next"]').trigger('click')

  expect(component.findAll('.focus-visible\\:outline-primary-500').length).toBe(1)
  expect(component.find('.focus-visible\\:outline-primary-500').text()).toContain('2')

  //Arrows
  expect(component.find('[aria-label="Prev"]').attributes('disabled')).toBeUndefined()
  expect(component.find('[aria-label="Next"]').attributes('disabled')).toBeUndefined()
})

test('TransactionPaginate click specific page', async () => {
  const component = await mountSuspended(TransactionPaginate, {
    props: {
      rowsPerPage: 2,
      totalRows: 8
    }
  })

  expect(component.findAll('.focus-visible\\:ring-primary-500')[2].text()).toContain('3')
  await component.findAll('.focus-visible\\:ring-primary-500')[2].trigger('click')

  expect(component.findAll('.focus-visible\\:outline-primary-500').length).toBe(1)
  expect(component.find('.focus-visible\\:outline-primary-500').text()).toContain('3')

  //Arrows
  expect(component.find('[aria-label="Prev"]').attributes('disabled')).toBeUndefined()
  expect(component.find('[aria-label="Next"]').attributes('disabled')).toBeUndefined()
})

test('TransactionPaginate click on last page', async () => {
  const component = await mountSuspended(TransactionPaginate, {
    props: {
      rowsPerPage: 2,
      totalRows: 6
    }
  })

  expect(component.findAll('.focus-visible\\:ring-primary-500')[2].text()).toContain('3')
  await component.findAll('.focus-visible\\:ring-primary-500')[2].trigger('click')

  expect(component.find('.focus-visible\\:outline-primary-500').text()).toContain('3')
  expect(component.find('[aria-label="Prev"]').attributes('disabled')).toBeUndefined()
  expect(component.find('[aria-label="Next"]').attributes('disabled')).toBeDefined()
})
