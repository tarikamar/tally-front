import { getAPIBase } from "~/composables/API/getAPIConfig"
import type { APIThirdPartiesUpdateType, APIThirdPartiesReadType } from "~/types/API/APIThirdParties"

export const thirdPartiesAPI: {
  getAll: () => Promise<APIThirdPartiesReadType[]>
  addOne: (name: string) => Promise<APIThirdPartiesReadType>
  deleteOne: (id: number) => Promise<void>
  editOne: (id: number, fields: APIThirdPartiesUpdateType) => Promise<APIThirdPartiesReadType>
} = {
  getAll: async (): Promise<APIThirdPartiesReadType[]> => {
    return await $fetch(`${getAPIBase()}/third-parties`)
  },
  addOne: async (name): Promise<APIThirdPartiesReadType> => {
    if (!name)
      throw Error('Missing name')

    return await $fetch(
      `${getAPIBase()}/third-parties`,
      {
        method: 'POST',
        body: {name}
      }
    )
  },
  deleteOne: async (id: number) => {
    if (!id)
      throw Error('Missing id')

    return await $fetch(`${getAPIBase()}/third-parties/${id}`, {method: 'DELETE'})
  },
  editOne: async (id: number, body: APIThirdPartiesUpdateType) => {
    if (!id)
      throw Error('Missing id')

    if (!body)
      throw Error('Missing body')

    const {name} = body

    if (!name)
      throw Error('Missing name')

    return await $fetch(
      `${getAPIBase()}/third-parties/${id}`,
      {method: 'PUT', body}
    )
  }
}
