export const getAPIBase = (): String => {
  const config = useRuntimeConfig()
  if (!config?.public?.apiBase)
    showError({
      statusCode: 500,
      statusMessage: 'API base url is not set in config'
    })

  return config.public.apiBase
}
