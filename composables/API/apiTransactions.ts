import type { APITransactionsUpdateType, APITransactionsReadType } from "~/types/API/APITransactions"
import { getAPIBase } from "~/composables/API/getAPIConfig"
import type { APIThirdPartiesCreateType } from "~/types/API/APIThirdParties"

export const transactionsAPI: {
  getAll: () => Promise<APITransactionsReadType[]>
  addOne: (date: string, amount: number, thirdParty: APIThirdPartiesCreateType) => Promise<APITransactionsReadType>
  deleteOne: (id: number) => Promise<void>
  editOne: (id: number, fields: APITransactionsUpdateType) => Promise<APITransactionsReadType>
} = {
  getAll: async (): Promise<APITransactionsReadType[]> => {
    return await $fetch(`${getAPIBase()}/transactions`)
  },

  addOne: async (date, amount, thirdPartyId): Promise<APITransactionsReadType> => {
    if (!date)
      throw Error('Missing date')

    if (!amount)
      throw Error('Missing amount')

    if (!thirdPartyId)
      throw Error('Missing third party id')

    return await $fetch(
      `${getAPIBase()}/transactions`,
      {
        method: 'POST',
        body: {date, amount, third_party_id: thirdPartyId}
      }
    )
  },

  deleteOne: async (id: number) => {
    if (!id)
      throw Error('Missing id')

    return await $fetch(`${getAPIBase()}/transactions/${id}`, {method: 'DELETE'})
  },

  editOne: async (id: number, body: APITransactionsUpdateType) => {
    if (!id)
      throw Error('Missing id')

    if (!body)
      throw Error('Missing body')

    const {date, amount, third_party_id} = body

    if (!date)
      throw Error('Missing date')

    if (!amount)
      throw Error('Missing amount')

    if (!third_party_id)
      throw Error('Missing third party id')

    return await $fetch(
      `${getAPIBase()}/transactions/${id}`,
      {
          method: 'PUT',
          body: {date, amount, third_party_id: third_party_id}
      }
    )
  }
}
