export default defineAppConfig({
  ui: {
    primary: 'green',
    gray: 'stone',
    notifications: {
      position: 'top-0 bottom-auto'
    }
  }
})
