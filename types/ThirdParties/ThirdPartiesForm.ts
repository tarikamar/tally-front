export type ThirdPartiesCreateFormType = {
  name: string
}

export type ThirdPartiesUpdateFormType = {
  id: number,
  name: string
}

export type ThirdPartiesReadFormType = {
  id: number,
  name: string
}
