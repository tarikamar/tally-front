export type ColumnType = {
  key: string
  label: string
  sortable?: boolean | undefined
  sort?: ((a: any, b: any, direction: "desc" | "asc") => number)
}
