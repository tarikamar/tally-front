import type { ThirdPartiesReadFormType } from "../ThirdParties/ThirdPartiesForm"

export type TransactionCreateFormType = {
  amount: number,
  date: string,
  "third-party": ThirdPartiesReadFormType
}

export type TransactionUpdateFormType = {
  id: number,
  amount: number,
  date: string,
  "third-party": number
}

export type TransactionReadFormType = {
  id: number,
  amount: number,
  date: string,
  "third-party": number // Certainly an api type
}

export type TransactionInitialFormType = {
  id: number | undefined,
  amount: number | undefined,
  date: string,
  "third-party": number | undefined
}
