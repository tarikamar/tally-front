import type { APITransactionsType } from "~/types/API/APITransactions";
export type TransactionsRowType = APITransactionsType & {class?: string}
