import type { APITransactionsPartialType } from "./APITransactions"

export type APIThirdPartiesCreateType = {
  name: string
}

export type APIThirdPartiesUpdateType = APIThirdPartiesCreateType

export type APIThirdPartiesReadType = {
  id: number,
  name: string,
  transactions: APITransactionsPartialType[]
}

export type APIThirdPartiesPartialType = {
  name: string,
  id: number,
}
