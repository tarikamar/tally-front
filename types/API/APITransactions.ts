import type { APIThirdPartiesPartialType } from "./APIThirdParties"

export type APITransactionsCreateType = {
  amount: number,
  date: string
  third_party_id: number
}

export type APITransactionsUpdateType = {
  amount: number,
  date: string
  third_party_id: number
}

export type APITransactionsReadType = {
  id: number,
  amount: number,
  date: string
  third_party: APIThirdPartiesPartialType
}

export type APITransactionsPartialType = {
  amount: number,
  date: string
}
