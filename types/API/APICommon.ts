export type APIErrorType = {
    detail: {
      loc: string[]
      type: string
    }[]
}
