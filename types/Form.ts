import type { APIErrorType } from '~/types/API/APICommon'

export type $FetchErrorType = Error & { data: APIErrorType }
export type NuxtUIFormError = {path: string, message: string}
