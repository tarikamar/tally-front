<script setup lang="ts">
import type { FormSubmitEvent } from '#ui/types'
import TransactionFormError from '~/components/Transaction/TransactionFormError'
import { transactionsAPI } from '~/composables/API/apiTransactions'
import type { $FetchErrorType, NuxtUIFormError } from '~/types/Form'
import type { APITransactionsReadType } from '~/types/API/APITransactions'
import type { APIThirdPartiesReadType } from '~/types/API/APIThirdParties'
import type {
  TransactionCreateFormType,
  TransactionInitialFormType,
  TransactionUpdateFormType
} from '~/types/Transactions/TransactionsForm'

const props = defineProps<{
  transaction: APITransactionsReadType | undefined, // Get it on edit
  thirdParties: APIThirdPartiesReadType | undefined // Get it on load
}>()

onMounted(() => {
  focusInputDate()
})

const defaultFormValues: TransactionInitialFormType = {
  id: undefined,
  amount: undefined,
  date: (new Date()).toJSON().slice(0, 10),
  "third-party": undefined // unselected third party
}
const formState = useState('formState', () => defaultFormValues)
const change    = useState('change', () => false)
const edit      = useState('edit', () => false)
const emit      = defineEmits(['transaction-added', 'transaction-edited'])
const form      = ref()
const inputDate: Component | null = ref(null)

watch(
  () => props.transaction,
  () => {
    if (props.transaction) {
      formState.value = {
          id: props.transaction.id,
          amount: props.transaction.amount,
          date: props.transaction.date,
          "third-party": props.transaction.third_party.id
      }
      change.value    = true // Form change
      edit.value      = true // Edit transaction
    }
  }
)

const validate = (formState: Record<string, number>): NuxtUIFormError[] => {
  const errors: NuxtUIFormError[] = []

  if (change)
    return errors

  if (!formState.date)   errors.push({path: 'date',   message: 'Date requise'})
  if (!formState.amount) errors.push({path: 'amount', message: 'Somme requise'})

  return errors
}

const resetForm = () => {
  formState.value = structuredClone(defaultFormValues)
  edit.value = false
  change.value = false
}

const warnError = inject('warnError') as (param: warnErrorParamType) => void

const onDebitHandler = () => {
  const {amount} = formState.value
  if (!amount)
    return

  formState.value.amount = Math.abs(amount) * -1
}

const onCreditHandler = () => {
  const {amount} = formState.value
  if (!amount)
    return

  formState.value.amount = Math.abs(amount)
}

const onSubmitHandler = async (
  event: FormSubmitEvent<TransactionCreateFormType | TransactionUpdateFormType>
): Promise<void> => {
  // Clears form errors associated with a specific path. If no path is provided, clears all form errors.
  form.value.clear()

  const payload = {...event.data}
  if (edit.value === true)
    return editTransaction(payload as TransactionUpdateFormType)

  createTransaction(payload as TransactionCreateFormType)
}

const checkMandatoryFields = (fields: TransactionCreateFormType | TransactionUpdateFormType) => {
  if (!fields.date)
    throw new TransactionFormError('missing date', 'Date manquante')

  if (!fields.amount)
    throw new TransactionFormError('missing amount', 'Somme manquante')

  if (!fields['third-party'])
    throw new TransactionFormError('missing third', 'Tier manquant')
}

const createTransaction = async (fields: TransactionCreateFormType) => {
  try {
    checkMandatoryFields(fields)
  } catch (e) {
    const {message, title} = e as TransactionFormError
    return warnError({message, title})
  }

  const {date, amount, "third-party": thirdParty} = fields

  try {
    await transactionsAPI.addOne(date, amount, thirdParty.id)
    postTransactionAction()
    emit('transaction-added')

  } catch (errors) {
    if (!(errors as $FetchErrorType).data) // Unhandled API error
      return warnError({
          message: 'Failed to fetch',
          title: 'L’envoie des informations a échoué'
      })

    form.value.setErrors((errors as $FetchErrorType).data.detail.map(error => ({
      path: error.loc[1],
      message: error.type === 'missing' ? 'Champs manquant' : 'Champs en erreur'
    })))
  }
}

const editTransaction = async (fields: TransactionUpdateFormType) => {
  try {
    checkMandatoryFields(fields)
  } catch (e) {
    const {message, title} = e as TransactionFormError
    return warnError({message, title})
  }

  if (!fields.id)
    return warnError({message: 'Missing id for edit'})

  const {
    id,
    date,
    amount,
    "third-party": thirdParty
  }: {
      id: number,
      date: string,
      amount: number,
      "third-party": number
    } = fields

  try {
    await transactionsAPI.editOne(
      id,
      {
        date,
        amount,
        third_party_id: thirdParty
      }
    )
    postTransactionAction()
    emit('transaction-edited')

  } catch (errors) {
    if (!(errors as $FetchErrorType).data) // Unhandled API error
      return warnError({message: 'Failed to fetch!', errors})

    form.value.setErrors((errors as $FetchErrorType).data.detail.map(error => ({
      path: error.loc[1],
      message: error.type === 'missing' ? 'Champs manquant' : 'Champs en erreur'
    })))
  }
}

const onReset = async () => resetForm()

const onChange = () => {
  if (formState.value.amount === 0)
    formState.value.amount = undefined

  change.value = JSON.stringify(formState.value) !== JSON.stringify(defaultFormValues)
}

const focusInputDate = () => {
  if (inputDate?.value?.input)
    inputDate.value.input.focus()
}

const postTransactionAction = () => {
  resetForm()
  focusInputDate()
}
</script>

<template>
  <UForm
    ref="form"
    :validate="validate"
    :state="formState"
    @submit="onSubmitHandler"
    @change="onChange"
    @reset="onReset"
  >
    <FormLayout>
      <template #form>
        <input type="hidden" v-model="formState.id" />
        <UFormGroup
          label="Date"
          name="date"
          required
        >
          <UInput
            type="date"
            v-model="formState.date"
            icon="i-heroicons-calendar-days-20-solid"
            ref="inputDate"
          />
        </UFormGroup>

        <UFormGroup
          label="Somme"
          name="amount"
          required
        >
          <UInput
            type="number"
            v-model="formState.amount"
            placeholder="Combien ?"
            icon="i-heroicons-banknotes-20-solid"
          >
            <template #trailing>
              <span class="text-gray-500 dark:text-gray-400 text-xs">EUR</span>
            </template>
          </UInput>
        </UFormGroup>
        <UFormGroup
          label="Tier"
          name="third-party"
          required
        >
          <USelectMenu
            :options="thirdParties"
            option-attribute="name"
            placeholder="Qui ?"
            v-model="formState['third-party']"
            searchable
            searchable-placeholder="Recherchez un tier"
            clear-search-on-close
            class="w-full lg:w-48"
            icon="i-heroicons-user-circle-20-solid"
          />
        </UFormGroup>
      </template>

      <template #submit>
        <TransactionFormSubmit
          :edit="edit"
          @debit="onDebitHandler"
          @credit="onCreditHandler"
        />
        <UButton 
          :disabled="!change"
          type="reset"
          color="gray"
          icon="i-heroicons-x-circle-20-solid"
          class="my-5"
        >
          Annuler
        </UButton>
      </template>
    </FormLayout>
  </UForm>
</template>
