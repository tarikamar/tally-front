export default class TransactionFormError extends Error {
  title: string

  constructor(message:string, title:string="Erreur technique") {
    super(message)

    this.title = title
  }
}
